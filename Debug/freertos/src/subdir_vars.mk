################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../freertos/src/croutine.c \
../freertos/src/event_groups.c \
../freertos/src/heap_2.c \
../freertos/src/list.c \
../freertos/src/queue.c \
../freertos/src/tasks.c \
../freertos/src/timers.c 

C_DEPS += \
./freertos/src/croutine.d \
./freertos/src/event_groups.d \
./freertos/src/heap_2.d \
./freertos/src/list.d \
./freertos/src/queue.d \
./freertos/src/tasks.d \
./freertos/src/timers.d 

OBJS += \
./freertos/src/croutine.obj \
./freertos/src/event_groups.obj \
./freertos/src/heap_2.obj \
./freertos/src/list.obj \
./freertos/src/queue.obj \
./freertos/src/tasks.obj \
./freertos/src/timers.obj 

OBJS__QUOTED += \
"freertos\src\croutine.obj" \
"freertos\src\event_groups.obj" \
"freertos\src\heap_2.obj" \
"freertos\src\list.obj" \
"freertos\src\queue.obj" \
"freertos\src\tasks.obj" \
"freertos\src\timers.obj" 

C_DEPS__QUOTED += \
"freertos\src\croutine.d" \
"freertos\src\event_groups.d" \
"freertos\src\heap_2.d" \
"freertos\src\list.d" \
"freertos\src\queue.d" \
"freertos\src\tasks.d" \
"freertos\src\timers.d" 

C_SRCS__QUOTED += \
"../freertos/src/croutine.c" \
"../freertos/src/event_groups.c" \
"../freertos/src/heap_2.c" \
"../freertos/src/list.c" \
"../freertos/src/queue.c" \
"../freertos/src/tasks.c" \
"../freertos/src/timers.c" 


