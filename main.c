//*****************************************************************************
//
// Copyright (C) 2015 - 2016 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//  Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//
//  Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the
//  distribution.
//
//  Neither the name of Texas Instruments Incorporated nor the names of
//  its contributors may be used to endorse or promote products derived
//  from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

// Includes standard
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

// Includes FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "msp.h"
#include <driverlib.h>
#include <grlib.h>

#include "Crystalfontz128x128_ST7735.h"
#include "HAL_MSP_EXP432P401R_Crystalfontz128x128_ST7735.h"

// Includes drivers
#include "i2c_driver.h"
#include "temp_sensor.h"
#include "opt3001.h"
#include "tmp006.h"
#include "accelerometer_driver.h"
#include "adc14_multiple_channel_no_repeat.h"

// Definicion de parametros RTOS
#define ENVIOLT_TASK_PRIORITY    2
#define LECTURA_TASK_PRIORITY  3
#define HEARTBEAT_TASK_PRIORITY 2
#define ENVIOACC_TASK_PRIORITY    2
#define T_COLATEMPLUZ 30
#define T_COLAACE 10
#define T_COLAMEDIAS 2
#define HALF_SECOND_MS  500
#define LECTURA_PERIOD_MS    10
#define ENVIO_PERIOD_MS    100
#define HEART_BEAT_ON_MS 10
#define HEART_BEAT_OFF_MS 990
#define TAM_STACK_T23 500
#define TAM_STACK_T4 800

// Definici�n cola que almacena datos de temperatura y luz.
xQueueHandle Cola_TempLuz;
// Definici�n cola que almacena datos del aceler�metro.
xQueueHandle Cola_Acele;
// Definici�n cola para calculo de medias.
xQueueHandle Cola_Medias;

// Definimos el contexto gr�fico.
Graphics_Context g_sContext;

//Definici�n variables para calculo de medias de luz,temperatura y aceler�metro.
volatile float MediaLuz=0;
volatile float MediaTemp1m=0;
volatile float MediaTemp2m=0;
volatile float DifTemp2Temp1=0;
volatile float sumatm1=0;
volatile float sumatm2=0;
float Mediaaccx;
float Mediaaccy;
float Mediaaccz;
// Variable global que almacena temperatura. Iniciamos con 20 grados.
float TemperaturaG=20.0;

// Definici�n estructura para los ejes del aceler�mtro
typedef struct {
    float acc_x;
    float acc_y;
    float acc_z;
} acc_t;

// Definici�n estructura para posteriormente saber si es una lectura de luz o temperatura.
typedef enum Sensor
{
    light = 1,
    temp = 2
} Sensor;

// Definici�n estructura para los sensores de Luz y Temperatura.
typedef struct {
    Sensor sensor;
    float value;
} Queue_reg_t;

// Definimos variable EleTempLuz.
Queue_reg_t EleTempLuz;

// Definici�n estructura con la que posteriormente controlaremos las dos ultimas medias en una cola.
typedef struct {
    float valor;
} Medias;
// Definimos variable EleMedias.
Medias EleMedias;
// Cambiamos en el LCD para identificar quien es el primer y �ltimo minuto.
char rotulo1[13]="MedPrimerMin:";
char rotulo2[13]="MedUltimoMin:";
char aux[13]="";

// Prototipos de funciones privadas
static void prvHeartBeatTask(void *pvParameters);
static void prvEnvioTempLuzTask(void *pvParameters);
static void prvEnvioAccTask(void *pvParameters);
static void prvLecturaTask(void *pvParameters);
// Prototipos de funciones inicializaci�n hardware.
static void prvSetupHardware(void);

// Declaraci�n de un mutex para el env�o a la cola de luz y temperatura.
SemaphoreHandle_t xMutexColaTempLuz;
// Declaraci�n de un mutex para el env�o a la cola de luz y temperatura.
SemaphoreHandle_t xMutexTemperatura;
// Declaracion de un semaforo binario sincronizaci�n ADC/aceler�metro y tarea EnvioAcc.
volatile SemaphoreHandle_t xBinarySemaphore;
// Declaracion de un semaforo binario sincronizacion escrituras y lecturas de Luz.
volatile SemaphoreHandle_t xSemaphoreluz;
// Declaracion de un semaforo binario sincronizacion escrituras y lecturas de Temperatura.
volatile SemaphoreHandle_t xSemaphoretemp;
// Declaracion de un semaforo binario sincronizacion escrituras y lecturas de Aceler�metro.
volatile SemaphoreHandle_t xSemaphoreacc;

int main(void)
{
    // Inicializacion del semaforo binario
    xBinarySemaphore = xSemaphoreCreateBinary();
    // Inicializacion del semaforo binario
    xSemaphoreluz= xSemaphoreCreateBinary();
    // Inicializacion del semaforo binario
    xSemaphoretemp= xSemaphoreCreateBinary();
    // Inicializacion del semaforo binario
    xSemaphoreacc= xSemaphoreCreateBinary();
    // Iniciamos Mutex para controlar la inserci�n en la cola.
    xMutexColaTempLuz= xSemaphoreCreateMutex();
    xMutexTemperatura=xSemaphoreCreateMutex();
    //Creaci�n cola temperatura luz.
    Cola_TempLuz=xQueueCreate(T_COLATEMPLUZ,sizeof(EleTempLuz));
    // Inicializacion de la cola
    Cola_Acele = xQueueCreate( T_COLAACE, sizeof( acc_t ) );
    // Inicializacion de la cola
    Cola_Medias = xQueueCreate( T_COLAMEDIAS, sizeof( EleMedias ) );

    // Comprueba si semaforo y mutex se han creado bien
    if ((xBinarySemaphore != NULL)  && (xSemaphoreluz != NULL) && (xSemaphoretemp != NULL) && (xSemaphoreacc != NULL) && (xMutexColaTempLuz != NULL) &&
            (xMutexTemperatura != NULL)&& (Cola_TempLuz != NULL)&& (Cola_Acele != NULL)&& (Cola_Medias != NULL))
    {
        // Inicializacion del hardware (clocks, GPIOs, IRQs)
        prvSetupHardware();

        // Creacion de tareas 1, 2, 3 y 4.
        xTaskCreate( prvHeartBeatTask, "HeartBeatTask", configMINIMAL_STACK_SIZE, NULL,HEARTBEAT_TASK_PRIORITY, NULL );
        xTaskCreate( prvEnvioTempLuzTask, "EnvioTempLuzTask", TAM_STACK_T23, NULL,ENVIOLT_TASK_PRIORITY, NULL );
        xTaskCreate( prvEnvioAccTask, "EnvioAccTask", TAM_STACK_T23, NULL, ENVIOACC_TASK_PRIORITY, NULL );
        xTaskCreate( prvLecturaTask, "LecturaTask", TAM_STACK_T4, NULL,LECTURA_TASK_PRIORITY, NULL );

        vTaskStartScheduler();
    }
    // Solo llega aqui si no hay suficiente memoria
    // para iniciar el scheduler
    return 0;
}

// Inicializacion del hardware del sistema
static void prvSetupHardware(void){

    extern void FPU_enableModule(void);

    // Configuracion del pin P1.0 - LED 1 - como salida y puesta en OFF
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);

    // Inicializacion de pins sobrantes para reducir consumo
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PB, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PE, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PB, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PE, PIN_ALL16);

    // Habilita la FPU
    MAP_FPU_enableModule();

    // Cambia el numero de "wait states" del controlador de Flash
    MAP_FlashCtl_setWaitState(FLASH_BANK0, 2);
    MAP_FlashCtl_setWaitState(FLASH_BANK1, 2);

    // Selecciona la frecuencia central de un rango de frecuencias del DCO
    MAP_CS_setDCOCenteredFrequency(CS_DCO_FREQUENCY_6);
    // Configura la frecuencia del DCO
    CS_setDCOFrequency(CS_8MHZ);

    // Inicializa los clocks HSMCLK, SMCLK, MCLK y ACLK
    MAP_CS_initClockSignal(CS_HSMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_ACLK, CS_REFOCLK_SELECT, CS_CLOCK_DIVIDER_1);

    // Selecciona el nivel de tension del core
    MAP_PCM_setCoreVoltageLevel(PCM_VCORE0);

    // Inicializacion del I2C
    I2C_init();
    // Inicializacion del sensor TMP006
    TMP006_init();
    // Inicializacion del sensor opt3001
    sensorOpt3001Init();
    sensorOpt3001Enable(true);

    // Inicializamos la pantalla.
    Crystalfontz128x128_Init();
    // Confiruamos por defecto la orientaci�n de la pantalla.
    Crystalfontz128x128_SetOrientation(0);
   // Inicializamos el contexto gr�fico.
    Graphics_initContext(&g_sContext, &g_sCrystalfontz128x128);
    Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_RED);
    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
    GrContextFontSet(&g_sContext, &g_sFontFixed6x8);
    Graphics_clearDisplay(&g_sContext);
    // Cuando se inicia, mandamos al LCD los siguientes r�tulos.
    Graphics_drawStringCentered(&g_sContext,"Practica",AUTO_STRING_LENGTH,64,8,OPAQUE_TEXT);
    Graphics_drawStringCentered(&g_sContext,"Media Ejex:",AUTO_STRING_LENGTH,37,22,OPAQUE_TEXT);
    Graphics_drawStringCentered(&g_sContext,"Media Ejey:",AUTO_STRING_LENGTH,37,37,OPAQUE_TEXT);
    Graphics_drawStringCentered(&g_sContext,"Media Ejez:",AUTO_STRING_LENGTH,37,52,OPAQUE_TEXT);
    Graphics_drawStringCentered(&g_sContext,"Media Luz:",AUTO_STRING_LENGTH,35,67,OPAQUE_TEXT);
    // R�tulo que iremos cambiendo en funci�n la �ltima media y la anterior.
    Graphics_drawStringCentered(&g_sContext,(int8_t *)rotulo1,13,42,82,OPAQUE_TEXT);
    // R�tulo que iremos cambiendo en funci�n la �ltima media y la anterior.
    Graphics_drawStringCentered(&g_sContext,(int8_t *)rotulo2,13,42,97,OPAQUE_TEXT);
    Graphics_drawStringCentered(&g_sContext,"DifMin2Min1:",AUTO_STRING_LENGTH,42,112,OPAQUE_TEXT);

      // Inicializa acelerometro+ADC
    init_Accel();
    // Habilita que el procesador responda a las interrupciones
    MAP_Interrupt_enableMaster();
}

// Tarea 1.
//Tarea heart beat.
static void prvHeartBeatTask (void *pvParameters){
    for(;;){
        // Fija a 1 la salida digital en pin 1.0 (LED on).
        MAP_GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN0);
        // Bloquea la tarea durante el tiempo de on del LED 10ms.
        vTaskDelay(pdMS_TO_TICKS(HEART_BEAT_ON_MS));
        // Fija a 0 la salida digital en pin 1.0 (LED off).
        MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);
        // Bloquea la tarea durante el tiempo de off del LED 990ms.
        vTaskDelay( pdMS_TO_TICKS(HEART_BEAT_OFF_MS));
    }
}
// Tarea 2.
// Tarea lectura de Temperatura y Luz.
static void prvEnvioTempLuzTask (void *pvParameters){
    // Variable que utilizaremos para agregar elementos a la cola Luz-Temp.
    Queue_reg_t queueLuz;
    // Variable que utilizaremos para agregar elementos a la cola Luz-Temp.
    Queue_reg_t queueTemp;
    uint16_t rawData;
    // Variable que utilizaremos para controlar el insertar en la cola la Temperatura cada 1 segundo y Luz cada medio segundo.
    uint8_t medioS=0;
    uint8_t unsegundo=0;
    uint8_t primeraL=0;
    uint8_t primeraT=0;
    float convertedLux;
    // La tarea se repite en un bucle infinito
    for(;;){
        // Cada medio segundo insertamos en la cola.
        if (medioS==1 || primeraL==0){
            // Sem�foro para sincronizar la escritura en la cola con la lectura en la tarea 4.
            xSemaphoreGive( xSemaphoreluz);
            // Lee el valor de la medida de luz
            sensorOpt3001Read(&rawData);
            sensorOpt3001Convert(rawData, &convertedLux);
            queueLuz.sensor = light;
            queueLuz.value = convertedLux;
            // Protegemos  cuando insertamos en la cola mediante mutex.
            if(xSemaphoreTake(xMutexColaTempLuz,0)){
                // Insertamos en la cola valores del sensor de Luz.
                xQueueSendToFront(Cola_TempLuz,&queueLuz,0);
            }
            // Liberamos el mutex
            xSemaphoreGive(xMutexColaTempLuz);
            medioS=0;
            primeraL=1;
        }
        // Cada un segundo insertamos en la cola.
        if (unsegundo==2 || primeraT==0){
            // Sem�foro para sincronizar la escritura en la cola con la lectura en la tarea 4.
            xSemaphoreGive( xSemaphoretemp);
            if(xSemaphoreTake(xMutexTemperatura,0)){
                // Lee el valor de la medida de temperatura.
                TemperaturaG = TMP006_readAmbientTemperature();
            }
            // Liberamos el mutex
            xSemaphoreGive(xMutexTemperatura);
            // Obtenemos valor de temperatura y fijamos en la variable de tipo struct que es una valor de temperatura.
            queueTemp.sensor = temp;
            queueTemp.value = TemperaturaG;
            // Protegemos  cuando insertamos en la cola mediante mutex.
            if(xSemaphoreTake(xMutexColaTempLuz,0)){
                // Insertamos en la cola valores del sensor de Temperatura.
                xQueueSendToFront(Cola_TempLuz,&queueTemp,0);
            }
            // Liberamos el mutex
            xSemaphoreGive(xMutexColaTempLuz);
            unsegundo=0;
            primeraT=1;
        }
        medioS++;
        unsegundo++;
        // Esperamos medio segundo.
        vTaskDelay(pdMS_TO_TICKS(HALF_SECOND_MS));
    }
}
// Tarea 3.
// Tarea que hace lecturas y env�a a la cola datos del aceler�metro.
static void prvEnvioAccTask (void *pvParameters)
{
    // Variable para controlar 30 segundos.
    uint16_t treintas=0;
    // Resultado del envio a la cola
    acc_t queue_element;
    float Datos[NUM_ADC_CHANNELS];
    // Toma el sem�foro procedente de la ISR para sincronizar ADC con la Tarea 3.
    xSemaphoreTake( xBinarySemaphore, 0 );
    // La tarea se repite en un bucle infinito
    for(;;) {
        treintas++;
        // Controlamos 30 segundos y hacemos una lectura de temperatura.
        if (treintas==300){
            // Mutex para controlar la variable global TemperaturaG.
            if(xSemaphoreTake(xMutexTemperatura,0)){
                TemperaturaG=TMP006_readAmbientTemperature();
            }
            // Liberamos el mutex.
            xSemaphoreGive(xMutexTemperatura);
            treintas=0;
        }
        // Llama a la funci�n Accel_read la cual lee datos del aceler�metro y convierte a fuerza G.
        Accel_read(Datos);
        // Sem�foro para sincronizar la escritura en la cola con la lectura en la tarea 4.
        xSemaphoreGive( xSemaphoreacc);
        // Guardamos en la variable queue_element los datos de los ejes.
        queue_element.acc_x = Datos[0];
        queue_element.acc_y = Datos[1];
        queue_element.acc_z = Datos[2];
        // Insertamos el frente de la cola. (ya que nos piden medidas de las �ltimas lecturas.)
        xQueueSendToFront(Cola_Acele, &queue_element, 0);
        // Esperamos 100 milisegundos.
        vTaskDelay(pdMS_TO_TICKS(ENVIO_PERIOD_MS));
    }
}
// Tarea 4.
//Tarea lectura de las colas de Luz-Temperatura y Aceler�metro.
static void prvLecturaTask (void *pvParameters)
{
    // Resultado del envio a la cola.
    Queue_reg_t queuetempluz;
    acc_t queue_element;
    // Resultado del envia a la cola de dos posiciones que guarda las medias.
    Medias queue_medias;
    // Definici�n variables de control para las medias de temperatura y diferencia de medias respecto a la anterior.
    uint8_t indexacc=0;
    uint8_t indexluz=0;
    uint8_t indextemp=0;
    uint8_t control=0;
    uint8_t controldif=0;
    uint8_t controldif2=0;
    for(;;){
        // Sacamos elemento de la cola TempLuz.
        xQueueReceive(Cola_TempLuz,&queuetempluz,0);
        // Sacamos elemento de la cola Aceler�metro.
        xQueueReceive(Cola_Acele, &queue_element, 0 );
        // Si es una lectura de luz.
        if (queuetempluz.sensor==light){
            // Sem�foro para sincronizar la tarea de escritura y lectura.
            if (xSemaphoreTake( xSemaphoreluz, 0 )){
                // Sumatorio de medidas de luz.
                MediaLuz+=queuetempluz.value;
                indexluz++;
                // Nos piden la media de 2 lecturas.
                if(indexluz==2){
                    // Media de las lecturas.
                    MediaLuz=MediaLuz/2;
                    indexluz=0;
                    // Imprimimos en el LCD la media de luz.
                    char string[6];
                    sprintf(string, "%f", MediaLuz);
                    Graphics_drawStringCentered(&g_sContext,(int8_t *)string,6,105,67,OPAQUE_TEXT);
                    MediaLuz=0;
                }
            }
        // Si es una lectura de temperatura.
        }else if(queuetempluz.sensor==temp){
            // Sem�foro para sincronizar la tarea de escritura y lectura.
            if (xSemaphoreTake( xSemaphoretemp, 0 )){
                // Variable que controla que lleguen dos medias de temperatura el primer minuto y el segundo.
                if (control==0){
                    // Sumatorio de muestras de temperatura.
                    sumatm1+=queuetempluz.value;
                    // Incremento de variable.
                    indextemp++;
                    // Si son 60 lecturas.
                    if(indextemp==60){
                        // Variable de control pasa a 1 y ejecutaria el siguiente condicionante de 60 muestras.
                        control=1;
                        // Media de muestras de temperatura.
                        MediaTemp1m=sumatm1/60;
                        // Cola de dos posciciones donde guardamos las medias del ultimo y anterior minituo.
                        queue_medias.valor=MediaTemp1m;
                        // Guardamos un elemento en la cola Medias.
                        xQueueSendToBack(Cola_Medias, &queue_medias, 0);
                        // Imprimimos media de temperatura en el LCD.
                        char stringtemp[6];
                        sprintf(stringtemp, "%f", MediaTemp1m);
                        Graphics_drawStringCentered(&g_sContext,(int8_t *)stringtemp,6,105,82,OPAQUE_TEXT);
                        // No entra en esta condici�n hasta el tercer minuto. Posteriormente alternaremos entre el ultimo y anterior valor.
                        if (controldif==1){
                           // Cambiamos a partir del 3 minuto en adelante indicando quien es el �ltimo y primer minuto.
                           strncpy(aux, rotulo1, 13);
                           strncpy(rotulo1, rotulo2, 13);
                           strncpy(rotulo2, aux, 13);
                           // Variable que guarda la diferencia de medias de temperatura. El elemento que entra en la cola de dos posiciones menos el anterior.
                           DifTemp2Temp1=queue_medias.valor-MediaTemp2m;
                           // Imprimimos los r�tulos indicando el �ltimo y primer minuto.
                           Graphics_drawStringCentered(&g_sContext,(int8_t *)rotulo1,13,42,82,OPAQUE_TEXT);
                           Graphics_drawStringCentered(&g_sContext,(int8_t *)rotulo2,13,42,97,OPAQUE_TEXT);
                           // Imprimimos media.
                           char stringdif[6];
                           sprintf(stringdif, "%f", DifTemp2Temp1);
                           Graphics_drawStringCentered(&g_sContext,(int8_t *)stringdif,6,105,112,OPAQUE_TEXT);
                           // Sacamos un elemento de la cola Medias.
                           xQueueReceive(Cola_Medias, &queue_medias, 0 );
                           MediaTemp2m=0;
                       }
                       controldif=1;
                       sumatm1=0;
                       indextemp=0;
                    }
                    // si la variable control es distinto de 0.
                }else{
                    // Sumatorio de muestras de temperatura.
                    sumatm2+=queuetempluz.value;
                    // Incremento de variable.
                    indextemp++;
                    // Si son 60 lecturas.
                    if (indextemp==60){
                        // Media de muestras de temperatura.
                        MediaTemp2m=sumatm2/60;
                        // Cola de dos posciciones donde guardamos las medias del ultimo y anterior minituo.
                        queue_medias.valor=MediaTemp2m;
                        // Guardamos un elemento en la cola Medias.
                        xQueueSendToBack(Cola_Medias, &queue_medias, 0);
                        // Imprimimos media de temperatura en el LCD.
                        char stringtemp2[6];
                        sprintf(stringtemp2, "%f", MediaTemp2m);
                        Graphics_drawStringCentered(&g_sContext,(int8_t *)stringtemp2,6,105,97,OPAQUE_TEXT);
                        // Sacamos un elemento de la cola Medias.
                        xQueueReceive(Cola_Medias, &queue_medias, 0 );
                        // No entra en esta condici�n hasta el tercer minuto. Posteriormente alternaremos entre el ultimo y anterior valor.
                        if (controldif2==1){
                            // Cambiamos a partir del 3 minuto en adelante indicando quien es el �ltimo y primer minuto.
                            strncpy(aux, rotulo1, 13);
                            strncpy(rotulo1, rotulo2, 13);
                            strncpy(rotulo2, aux, 13);
                            // Imprimimos los r�tulos indicando el �ltimo y primer minuto.
                            Graphics_drawStringCentered(&g_sContext,(int8_t *)rotulo1,13,42,82,OPAQUE_TEXT);
                            Graphics_drawStringCentered(&g_sContext,(int8_t *)rotulo2,13,42,97,OPAQUE_TEXT);
                        }
                        controldif2=1;
                        // Variable que guarda la diferencia de medias de temperatura. El elemento que entra en la cola de dos posiciones menos el anterior.
                        DifTemp2Temp1=MediaTemp2m-queue_medias.valor;
                        // Imprimimos media.
                        char stringdif[6];
                        sprintf(stringdif, "%f", DifTemp2Temp1);
                        Graphics_drawStringCentered(&g_sContext,(int8_t *)stringdif,6,105,112,OPAQUE_TEXT);
                        DifTemp2Temp1=0;
                        control=0;
                        indextemp=0;
                        sumatm2=0;
                        sumatm1=0;
                        MediaTemp1m=0;
                    }
                }
            }
        }
        // Sem�foro para sincronizar la tarea de escritura y lectura.
        if (xSemaphoreTake( xSemaphoreacc, 0 )){
            // Sumatorios de los tres ejes del aceler�metro.
            Mediaaccx+=queue_element.acc_x;
            Mediaaccy+=queue_element.acc_y;
            Mediaaccz+=queue_element.acc_z;
            // Incrementamos �ndice.
            indexacc++;
            if (indexacc==10){
                // Calculamos media de los �ltimos 10 valores.
                Mediaaccx=Mediaaccx/10;
                Mediaaccy=Mediaaccy/10;
                Mediaaccz=Mediaaccz/10;
                indexacc=0;
                // Imprimimos media del eje x.
                char stringaccx[6];
                sprintf(stringaccx, "%f", Mediaaccx);
                Graphics_drawStringCentered(&g_sContext,(int8_t *)stringaccx,6,105,22,OPAQUE_TEXT);
                // Imprimimos media del eje y.
                char stringaccy[6];
                sprintf(stringaccy, "%f", Mediaaccy);
                Graphics_drawStringCentered(&g_sContext,(int8_t *)stringaccy,6,105,37,OPAQUE_TEXT);
                // Imprimimos media del eje z.
                char stringaccz[6];
                sprintf(stringaccz, "%f", Mediaaccz);
                Graphics_drawStringCentered(&g_sContext,(int8_t *)stringaccz,6,105,52,OPAQUE_TEXT);
            }
        }
        // Esperamos 100 milisegundos.
       vTaskDelay(pdMS_TO_TICKS(LECTURA_PERIOD_MS));
    }
}






