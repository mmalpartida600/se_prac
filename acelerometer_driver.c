/*
 * acelerometer_driver.c

 */

// Includes standard
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "accelerometer_driver.h"
#include "adc14_multiple_channel_no_repeat.h"

// Variable declarada como global en main.c
extern float TemperaturaG;

void init_Accel(void){
    init_ADC();
}

void Accel_read(float *values){
    uint16_t *Data;
    uint8_t i;
    float tempe=0;
    //pedir datos ADC
    Data = ADC_read();
    //tempe = TMP006_readAmbientTemperature();
    // Conversión a fuerza G.
    for (i=0;i<NUM_ADC_CHANNELS;i++){
        tempe=TemperaturaG;
        switch( i )
        {
            case 0:
                values[i]=(((((float)Data[i]*3.3)/16383)-1.65+(-0.01911))/(0.66*(1+0.0001+(-0.00069612)*tempe)))+0.0007*tempe;
            case 1 :
                values[i]=(((((float)Data[i]*3.3)/16383)-1.65+(-0.010924))/(0.66*(1+0.0001+(0.00069438)*tempe)))+0.0007*tempe;
            case 2 :
                values[i]=(((((float)Data[i]*3.3)/16383)-1.65+0.001)/(0.66*(1+(0.0004+(0.0001)*tempe))))+0.0007*tempe;
        }
    }
}




